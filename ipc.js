const myfs = require('./fs');
const configureIPC = (ipcMain, mainWindow)=>{
    ipcMain.on("project-list-request", (event) => {
        myfs.getProjectList().then(res=>{
            if(res.success){
                let projects = Object.keys(res.data) || [];
                mainWindow.webContents.send("project-list-response", projects);
            }
        });
    });
    ipcMain.on("create-project-request", (event, projectName, metadata)=>{
        myfs.createProject(projectName, metadata).then((res)=>{
            if(res.success){
                mainWindow.webContents.send("create-project-response", projectName, metadata);
            }
        });
    });
    ipcMain.on("create-request-request", (event, projectName, request)=>{
        myfs.createRequest(projectName, request).then((res)=>{
            if(res.success){
                mainWindow.webContents.send("create-request-response", res);
            }
        });
    });
    ipcMain.on("get-requests-request", (event, projectName)=>{
        myfs.getRequests(projectName).then((res)=>{
            if(res.success){
                mainWindow.webContents.send("get-requests-response", res);
            }
        });
    });
    ipcMain.on("save-request-request", (event, projectName, requestName, request)=>{
        myfs.saveRequest(projectName, requestName, request).then((res)=>{
            if(res.success){
                mainWindow.webContents.send("save-request-response", res);
            }
        });
    });
};

module.exports = configureIPC;