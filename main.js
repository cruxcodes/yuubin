const electron = require('electron');
const { app, BrowserWindow, ipcMain, Menu } = electron;
require('./fs');

let mainWindow;

function createMainWindow() {
    mainWindow = new BrowserWindow({
        webPreferences: {
            nodeIntegration: true
        }
    });
    require('./ipc')(ipcMain, mainWindow);
    mainWindow.loadURL(`file://${__dirname}/app/yuubin/dist/app/index.html`);

    const menuTemplate = [
        {
            label: 'File',
            submenu: [
                {
                    label: 'Quit',
                    accelerator: 'Ctrl+Q',
                    click() {
                        app.quit();
                    }
                }
            ]
        },
        {
            label: 'View',
            submenu: [
                {
                    label: 'Reload',
                    accelerator: 'F5',
                    click() {
                        mainWindow.loadURL(`file://${__dirname}/app/yuubin/dist/app/index.html`);
                    }
                }
            ]
        },
        {
            label: 'Window',
            submenu: [
                {
                    label: 'Developers Tool',
                    accelerator: 'Ctrl+I',
                    click() {
                        mainWindow.toggleDevTools();
                    }
                }
            ]
        }
    ];
    const menu = Menu.buildFromTemplate(menuTemplate);
    Menu.setApplicationMenu(menu);

    mainWindow.on('closed', () => {
        mainWindow = null;
    });
}

app.on('ready', () => {
    createMainWindow();
});