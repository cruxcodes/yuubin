import { createReducer, on, Action } from '@ngrx/store';
import * as fromNavActions from './navigation.action';

export interface NavState {
    active_nav: string;
}

export const initialNavState: NavState = {
    active_nav: "project"
}

const navReducer = createReducer(
    initialNavState,
    on(fromNavActions.ChangeNav, (state, payload)=>{
        return { ...state, active_nav: payload.active_nav };
    })
)

export function reducer(state: NavState, action: Action){
    return navReducer(state, action);
}