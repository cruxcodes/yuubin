import { Component, OnInit, AfterViewInit } from '@angular/core';
import * as $ from 'jquery';
import { Store } from '@ngrx/store';
import { navSelector, State } from '../../reducers';
import { NavState } from './store/navigation.reducer';
import { ChangeNav } from './store/navigation.action';
@Component({
    selector: "navigation",
    templateUrl: "./navigation.component.html",
    styleUrls: ["./navigation.component.scss"]
})
export class NavigationComponent implements AfterViewInit, OnInit {
    public mode: boolean = true;
    public dark: boolean = true;

    constructor(private store: Store<State>) {

    }
    ngOnInit(): void {
        this.store.select('navState').subscribe((state)=>{
        });
    }
    ngAfterViewInit(): void {
        $('ol').on('click', "li", this.handleNav.bind(this));
    }
    public handleNav($event) {
        switch ($event.currentTarget.value) {
            case 0:
                this.store.dispatch(ChangeNav({active_nav: "project"}));
                break;
            case 1:
                this.store.dispatch(ChangeNav({active_nav: "settings"}));
                break;
            default:
                console.log("default: no option chosen");
        }
    }
}