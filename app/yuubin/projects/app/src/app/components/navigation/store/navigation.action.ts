import { createAction, props } from '@ngrx/store';

export const CHANGE_NAV = "[NAVIGATION] CHANGE NAV";

export const ChangeNav = createAction(
    CHANGE_NAV,
    props<{active_nav: string}>()
);