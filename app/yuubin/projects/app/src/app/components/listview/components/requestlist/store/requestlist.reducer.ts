import { createReducer, on, Action } from '@ngrx/store';
import * as fromRequestActions from './requestlist.action';

export interface RequestListState {
    request_list: string[];
}

export const initialListState: RequestListState = {
    request_list: []
}

const requestlistReducer = createReducer(
    initialListState,
    on(fromRequestActions.UpdateRequestlist, (state, payload)=>{
        return { ...state, request_list: payload.request_list };
    })
)

export function reducer(state: RequestListState, action: Action){
    return requestlistReducer(state, action);
}