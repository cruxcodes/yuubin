import { createReducer, on, Action } from '@ngrx/store';
import * as fromListActions from './listview.action';

export interface ListState {
    active_list: string;
}

export const initialListState: ListState = {
    active_list: "project"
}

const listReducer = createReducer(
    initialListState,
    on(fromListActions.ChangeList, (state, payload)=>{
        return { ...state, active_list: payload.active_list };
    })
)

export function reducer(state: ListState, action: Action){
    return listReducer(state, action);
}