import { createAction, props } from '@ngrx/store';

export const CHANGE_LIST = "[LISTVIEW] CHANGE LIST";

export const ChangeList = createAction(
    CHANGE_LIST,
    props<{active_list: string}>()
);