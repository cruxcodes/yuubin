import { Component, OnInit, Inject, ComponentFactoryResolver, ViewContainerRef, ViewChild, AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../../reducers';
import { ProjectlistComponent } from './components/projectlist/projectlist.component';
import { SettingslistComponent } from './components/settingslist/settingslist.component';
import { RequestlistComponent } from './components/requestlist/requestlist.component';
import * as fromListActions from './store/listview.action';

@Component({
    selector: "listview",
    templateUrl: "./listview.component.html",
    styleUrls: ["./listview.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListViewComponent implements OnInit, AfterViewInit {
    private active_nav: string;
    private active_list: string;
    private factoryResolver: ComponentFactoryResolver;
    @ViewChild('dynamic', { read: ViewContainerRef }) viewContainerRef: ViewContainerRef;
    private projectlistComponent: any;
    private settingslistComponent: any;
    private requestlistComponent: any;
    constructor(private store: Store<State>, @Inject(ComponentFactoryResolver) factoryResolver: ComponentFactoryResolver, private ref: ChangeDetectorRef) {
        this.factoryResolver = factoryResolver;
    }
    ngOnInit(): void {
    }
    ngAfterViewInit(): void {
        const projectfactory = this.factoryResolver.resolveComponentFactory(ProjectlistComponent);
        this.projectlistComponent = projectfactory.create(this.viewContainerRef.parentInjector);
        const settingsfactory = this.factoryResolver.resolveComponentFactory(SettingslistComponent);
        this.settingslistComponent = settingsfactory.create(this.viewContainerRef.parentInjector);
        const requestfactory = this.factoryResolver.resolveComponentFactory(RequestlistComponent);
        this.requestlistComponent = requestfactory.create(this.viewContainerRef.parentInjector);
        this.store.select('navState').subscribe((state) => {
            if (this.active_nav != state.active_nav) {
                this.active_nav = state.active_nav;
                this.store.dispatch(fromListActions.ChangeList({active_list: this.active_nav}));
            }
        });
        this.store.select('listState').subscribe((state)=>{
            if (this.active_list != state.active_list){
                this.active_list = state.active_list;
                this.changeList();
            }
        });
    }
    changeList() {
        if (this.active_list == "project") {
            this.addDynamicComponent(this.projectlistComponent);
        }
        if (this.active_list == "settings") {
            this.addDynamicComponent(this.settingslistComponent);
        }
        if (this.active_list == "request") {
            this.addDynamicComponent(this.requestlistComponent);
        }
    }
    addDynamicComponent(component) {
        if(this.viewContainerRef.length > 0){
            this.viewContainerRef.detach(0);
        }
        this.viewContainerRef.insert(component.hostView);
        this.ref.detectChanges();
    }
}