import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { IpcRenderer } from 'electron';
import { Store } from '@ngrx/store';
import { State } from 'projects/app/src/app/reducers';
import { UpdateProjectlist } from './store/projectlist.action';
import * as $ from 'jquery';
import * as fromListActions from '../../store/listview.action';
import * as fromProjectActions from './store/projectlist.action';
@Component({
  'selector': "projectlist",
  'templateUrl': 'projectlist.component.html',
  'styleUrls': ['projectlist.component.scss']
})
export class ProjectlistComponent implements OnInit {
  private listEls = [];       // this is what is actually visible
  public project_list = [];  // this is what comes from store
  private ipc: IpcRenderer;
  public isVisible: boolean = false;
  @ViewChild('projectNameInput') projectNameInput: ElementRef;
  constructor(private store: Store<State>, private ref: ChangeDetectorRef) {
    if ((<any>window).require) {
      try {
        this.ipc = (<any>window).require('electron').ipcRenderer;
        this.ipc.on("project-list-response", (event, project_list) => {
          console.log(project_list);
          this.store.dispatch(UpdateProjectlist({ project_list }));
        });
        this.ipc.on("create-project-response", (event, projectName, metadata)=>{
          console.log("project got created", projectName, metadata);
          this.getProjectlist();
        });
      } catch (e) {
        throw e;
      }
    } else {
      console.warn('App not running inside Electron!');
    }
    this.getProjectlist();
  }
  ngOnInit(): void {
    this.store.select('projectState').subscribe((state) => {
      console.log("before check")
      if (!this.compareProjectList(state.project_list, this.project_list)) {
        
        this.project_list = state.project_list;
        console.log("after check", this.project_list);
        this.ref.detectChanges();
      }
    });
  }
  compareProjectList(a: string[], b: string[]): boolean {
    if (a.length != b.length) {
      return false;
    }
    if (a == undefined || b == undefined) {
      return false;
    }
    let temp = {};
    for (let i = 0; i < a.length; i++) {
      temp[a[i]] = true;
    }
    for (let i = 0; i < b.length; i++) {
      if (temp[b[i]] == undefined) {
        return false;
      }
    }
    return true;
  }
  getProjectlist() {
    this.ipc.send("project-list-request");
  }
  openNewProjectModal($event) {
    console.log("New project button clicked");
    this.isVisible = true;
 
  }
  handleOk(): void {
    console.log((<any>this.projectNameInput.nativeElement).value);
    let projectName = (<any>this.projectNameInput.nativeElement).value;
    this.ipc.send("create-project-request", projectName, {});
    this.isVisible = false;
    (<any>this.projectNameInput.nativeElement).value = "";
  }
  handleCancel(): void {
    this.isVisible = false;
  }
  modalLoaded(){
    console.log("modalLoaded called");
    setTimeout(()=>{
      document.getElementById("modalInput").focus();
      (<any>document.getElementById("modalInput")).select();
      $('#modalInput').focus();
    }, 300);
  }
  openProject($event, projectName){
    console.log(projectName);
    this.store.dispatch(fromProjectActions.SetActiveProject({project: projectName}));
    this.store.dispatch(fromListActions.ChangeList({active_list: "request"}));
  }
}