import { createAction, props } from '@ngrx/store';

export const UPDATE_REQUEST_LIST = "[RequestList] UPDATE REQUEST LIST";

export const UpdateRequestlist = createAction(
    UPDATE_REQUEST_LIST,
    props<{request_list: string[]}>()
);

export const SetActiveProject = createAction(
    UPDATE_REQUEST_LIST,
    props<{request_list: string}>()
)