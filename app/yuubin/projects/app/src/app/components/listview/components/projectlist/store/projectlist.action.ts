import { createAction, props } from '@ngrx/store';

export const UPDATE_PROJECT_LIST = "[ProjectList] UPDATE PROJECT LIST";
export const SET_ACTIVE_PROJECT = "[ProjectList] SET ACTIVE PROJECT";

export const UpdateProjectlist = createAction(
    UPDATE_PROJECT_LIST,
    props<{project_list: string[]}>()
);

export const SetActiveProject = createAction(
    SET_ACTIVE_PROJECT,
    props<{project: string}>()
)