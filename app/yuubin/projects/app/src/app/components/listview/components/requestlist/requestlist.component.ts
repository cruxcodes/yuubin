import { Component, ViewChild, ElementRef, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { State } from 'projects/app/src/app/reducers';
import { Store } from '@ngrx/store';
import { IpcRenderer } from 'electron';
import * as fromListActions from '../../store/listview.action';
import * as $ from 'jquery';
import * as fromRequestAction from './store/requestlist.action';
@Component({
    'selector': "requestlist",
    'templateUrl': 'requestlist.component.html',
    'styleUrls': ['requestlist.component.scss']
})
export class RequestlistComponent implements AfterViewInit {
    active_project: string;
    private ipc: IpcRenderer;
    @ViewChild('requestNameInput') requestNameInput: ElementRef;
    isVisible: boolean = false;
    request_list: string[] = [];
    constructor(private store: Store<State>, private ref: ChangeDetectorRef) {
        this.ipc = (<any>window).require('electron').ipcRenderer;
        this.ipc.on('create-request-response', (event, res) => {
            if (res.success) {
                console.log(res.data);
                this.getRequestsIPC();
            }
        });
        this.ipc.on('get-requests-response', (event, res) => {
            if (res.success) {
                console.log(res.data);
                this.store.dispatch(fromRequestAction.UpdateRequestlist({request_list: Object.keys(res.data)}))
            }
        });
        // this.ipc.on('save-request-response', (event, res)=>{
        //     if(res.success){
        //         console.log(res.data);
        //     }
        // });
        this.store.select('projectState').subscribe((state) => {
            if (this.active_project != state.active_project) {
                this.active_project = state.active_project;
                this.getRequestsIPC();
            }
        });
    }
    ngAfterViewInit(): void {
        this.store.select('requestState').subscribe((state)=>{
            this.request_list = state.request_list;
            this.ref.detectChanges();
        });
    }
    createRequestIPC(request = { name: 'default' }) {
        this.ipc.send('create-request-request', this.active_project, request);
    }
    getRequestsIPC() {
        this.ipc.send('get-requests-request', this.active_project);
    }
    onBack() {
        this.store.dispatch(fromListActions.ChangeList({ active_list: 'project' }));
    }
    openNewRequestModal($event) {
        console.log("New request button clicked");
        this.isVisible = true;
    }
    handleOk(): void {
        console.log((<any>this.requestNameInput.nativeElement).value);
        let requestName = (<any>this.requestNameInput.nativeElement).value;
        this.createRequestIPC({ name: requestName });
        this.isVisible = false;
        (<any>this.requestNameInput.nativeElement).value = "";
    }
    handleCancel(): void {
        this.isVisible = false;
    }
    modalLoaded() {
        console.log("modalLoaded called");
        setTimeout(() => {
            document.getElementById("modalInput").focus();
            (<any>document.getElementById("modalInput")).select();
            $('#modalInput').focus();
        }, 300);
    }
    openRequest($event, item){
        console.log("openRequest", item);
    }
}