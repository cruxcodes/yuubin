import { createReducer, on, Action } from '@ngrx/store';
import * as fromProjectActions from './projectlist.action';

export interface ProjectlistState {
    project_list: string[];
    active_project: string;
}

export const initialListState: ProjectlistState = {
    project_list: [],
    active_project: ""
}

const projectlistReducer = createReducer(
    initialListState,
    on(fromProjectActions.UpdateProjectlist, (state, payload)=>{
        return { ...state, project_list: payload.project_list };
    }),
    on(fromProjectActions.SetActiveProject, (state, payload)=>{
        return { ...state, active_project: payload.project };
    })
)

export function reducer(state: ProjectlistState, action: Action){
    return projectlistReducer(state, action);
}