import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';

import * as fromNav from '../components/navigation/store/navigation.reducer';
import * as fromList from '../components/listview/store/listview.reducer';
import * as fromProject from '../components/listview/components/projectlist/store/projectlist.reducer';
import * as fromRequest from '../components/listview/components/requestlist/store/requestlist.reducer';

export interface State {
  navState: fromNav.NavState,
  listState: fromList.ListState,
  projectState: fromProject.ProjectlistState,
  requestState: fromRequest.RequestListState
}

export const reducers: ActionReducerMap<State> = {
  navState: fromNav.reducer,
  listState: fromList.reducer,
  projectState: fromProject.reducer,
  requestState: fromRequest.reducer
};


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];

export const navSelector = (state: State)=>{state.navState};