import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './reducers';
import {
  HomeComponent,
  NavigationComponent,
  ListViewComponent,
  WorkviewComponent
} from './components';
import { 
  NzButtonModule,
  NzMenuModule,
  NzIconModule,
  NzPageHeaderModule,
  NzModalModule,
  NzInputModule,
  NzListModule
} from 'ng-zorro-antd';
import { ProjectlistComponent } from './components/listview/components/projectlist/projectlist.component';
import { SettingslistComponent } from './components/listview/components/settingslist/settingslist.component';
import { RequestlistComponent } from './components/listview/components/requestlist/requestlist.component';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavigationComponent,
    ListViewComponent,
    WorkviewComponent,
    ProjectlistComponent,
    SettingslistComponent,
    RequestlistComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    StoreModule.forRoot(reducers, {
      metaReducers
    }),
    NzButtonModule,
    NzMenuModule,
    NzIconModule,
    NzPageHeaderModule,
    NzModalModule,
    NzInputModule,
    NzListModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [ProjectlistComponent, SettingslistComponent, RequestlistComponent]
})
export class AppModule { }