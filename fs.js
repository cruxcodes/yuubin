const fs = require('fs');
const path = require('path');
const editJsonFile = require("edit-json-file");
const AppDataDir = "AppData";
const ProjectListFile = "projects.json";
const openProjectFiles = {};
if(!fs.existsSync(path.join(__dirname, AppDataDir))){
    fs.mkdirSync(path.join(__dirname, AppDataDir));
}
if(!fs.existsSync(path.join(__dirname, AppDataDir, ProjectListFile))){
    fs.writeFileSync(path.join(__dirname, AppDataDir, ProjectListFile), '');
}
let file = editJsonFile(path.join(__dirname, AppDataDir, ProjectListFile));
function createProject(projectName, metadata){
    let promise = new Promise((resolve)=>{
        fs.writeFileSync(path.join(__dirname, AppDataDir, projectName+'.json'), '');
        file.set(projectName, metadata);
        file.save(()=>{
            resolve({success: true});
        })
    });
    return promise;
}
function getProjectList(){
    let promise = new Promise((resolve)=>{
        resolve({success: true, data: file.get()});
    });
    return promise;
}
function createRequest(projectName, request){
    let promise = new Promise((resolve)=>{
        let project_file;
        if(!openProjectFiles[path.join(__dirname, AppDataDir, projectName+'.json')]){
            openProjectFiles[path.join(__dirname, AppDataDir, projectName+'.json')] = editJsonFile(path.join(__dirname, AppDataDir, projectName+'.json'));
        }
        project_file = openProjectFiles[path.join(__dirname, AppDataDir, projectName+'.json')];
        project_file.set(request.name, request);
        project_file.save(()=>{
            resolve({success: true, data: {projectName, request}});
        });
    });
    return promise;
}
function getRequests(projectName){
    let promise = new Promise(resolve=>{
        let project_file;
        if(!openProjectFiles[path.join(__dirname, AppDataDir, projectName+'.json')]){
            openProjectFiles[path.join(__dirname, AppDataDir, projectName+'.json')] = editJsonFile(path.join(__dirname, AppDataDir, projectName+'.json'));
        }
        project_file = openProjectFiles[path.join(__dirname, AppDataDir, projectName+'.json')];
        resolve({success: true, data: project_file.get()});
    });
    return promise;
}
function saveRequest(projectName, requestName, request){
    let promise = new Promise(resolve=>{
        let project_file;
        if(!openProjectFiles[path.join(__dirname, AppDataDir, projectName+'.json')]){
            openProjectFiles[path.join(__dirname, AppDataDir, projectName+'.json')] = editJsonFile(path.join(__dirname, AppDataDir, projectName+'.json'));
        }
        project_file = openProjectFiles[path.join(__dirname, AppDataDir, projectName+'.json')];
        project_file.set(requestName, request);
        resolve({success: true, data: {projectName, request}});
    });
    return promise;
}
module.exports = {
    createProject,
    getProjectList,
    createRequest,
    getRequests,
    saveRequest
}